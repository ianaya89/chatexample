angular.module('chatController', [])

.controller('ChatController', function($scope, Chat){
  //var socket = io.connect();

  $scope.messages = [];
  $scope.roster = [];
  $scope.name = '';
  $scope.text = '';

  Chat.connect(function(){
    $scope.setName();
  });

  Chat.message(function(msg){
    $scope.messages.push(msg);
    $scope.$apply();
  });

  Chat.roster(function(names){
    $scope.roster = names;
    $scope.$apply();
  });



  $scope.send = function send() {
    //console.log('Sending message:', $scope.text);
    Chat.sendMessage( $scope.text);
    //socket.emit('message', $scope.text);
    $scope.text = '';
  };

  $scope.setName = function setName() {
    Chat.setName( $scope.name);

  };
});
