var appChat = angular.module('appChat', ['chatController', 'chatService', 'ui.router']);

appChat.config(function ($stateProvider, $urlRouterProvider){

  $urlRouterProvider.otherwise('/index');

  $stateProvider

    //main
    .state('index',{
      url: '/index',
      templateUrl : "/views/index.html"
    });

})
