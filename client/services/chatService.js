angular.module('chatService', [])

.factory('Chat', function(){
  var socket = io.connect();

  return {
    connect :  function(callback){
      socket.on('connect', function () {
        callback();
      });
    },
    message: function (callback){
      socket.on('message', function (msg) {
        callback(msg);
      });
    },
    roster: function(callback){
      socket.on('roster', function (names) {
        callback(names);
      });
    },
    sendMessage: function(msg){
      socket.emit('message', msg);
    },
    setName: function(name){
      socket.emit('identify',name);
    }
  }
});
